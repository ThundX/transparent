﻿using System;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO;

namespace transparent
{
    public partial class MainWindow : Window
    {
        /// <summary>
        /// The current image to be edited. May be null if none is open.
        /// </summary>
        public ImageTransformer CurrentTransformer { get; set; }
        /// <summary>
        /// The sensitivity to be used in the background removal algorithm
        /// </summary>
        public int Sensitivity { get; set; }

        public MainWindow()
        {
            Sensitivity = 0;
#if DEBUG
            PresentationTraceSources.DataBindingSource.Switch.Level = SourceLevels.All;
#endif
            DataContext = this;
            InitializeComponent();
        }

        private void updateCurrentView()
        {
            if (CurrentTransformer == null)
            {
                ViewPanel.Visibility = Visibility.Collapsed;
                OpenPanel.Visibility = Visibility.Visible;
            }
            else
            {
                ViewPanel.Visibility = Visibility.Visible;
                OpenPanel.Visibility = Visibility.Collapsed;
                CurrentView.Source = CurrentTransformer.NewImage;
            }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog() { CheckFileExists = true, Multiselect = false };
            if (dialog.ShowDialog() == true)
            {
                openFile(dialog.FileName);
            }
        }

        private void CurrentView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (CurrentTransformer == null) return;
            var pos = e.GetPosition(CurrentView);
            Point transformedPos = new Point(pos.X * (double)CurrentTransformer.NewImage.PixelWidth / CurrentView.ActualWidth,
                pos.Y * (double)CurrentTransformer.NewImage.PixelHeight / CurrentView.ActualHeight);
            CurrentTransformer.RemoveBackgroundOn(transformedPos, Sensitivity);
            updateCurrentView();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTransformer == null) return;
            var dialog = new SaveFileDialog() { DefaultExt = ".png", AddExtension = true, Filter = "PNG-files|*.png" };
            if (dialog.ShowDialog() == true)
            {
                try
                {
                    CurrentTransformer.Save(dialog.FileName);
                }
                catch (IOException)
                {
                    MessageBox.Show("The image could not be saved.");
                }
            }
        }

        private void RevertButton_Click(object sender, RoutedEventArgs e)
        {
            CurrentTransformer.Reset();
            updateCurrentView();
        }

        private void BlackBackgroundCB_Checked(object sender, RoutedEventArgs e)
        {
            BlackBackground.Visibility = Visibility.Visible;
        }

        private void BlackBackgroundCB_Unchecked(object sender, RoutedEventArgs e)
        {
            BlackBackground.Visibility = Visibility.Collapsed;
        }

        private void openFile(String file)
        {
            try
            {
                CurrentTransformer = new ImageTransformer(file);
                updateCurrentView();
            }
            catch (FileFormatException)
            {
                MessageBox.Show("The requested file format is not supported.");
            }
            catch (IOException)
            {
                MessageBox.Show("The file could not be opened.");
            }
        }

        private void OpenPanel_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                var file = files[0];
                openFile(file);
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            CurrentTransformer = null;
            updateCurrentView();
        }
    }
}
