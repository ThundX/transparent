﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace transparent
{
    public class ImageTransformer
    {
        public BitmapSource Image { get; }
        public WriteableBitmap NewImage { get; private set; }

        public ImageTransformer(String path)
        {
            var decoder = BitmapDecoder.Create(new Uri(path, UriKind.RelativeOrAbsolute), BitmapCreateOptions.None, BitmapCacheOption.Default);
            BitmapSource origSrc = new FormatConvertedBitmap(decoder.Frames[0], PixelFormats.Bgra32, null, 1);
            int stride = origSrc.PixelWidth * origSrc.Format.BitsPerPixel;
            byte[] pixelData = new byte[origSrc.PixelHeight * stride];
            origSrc.CopyPixels(pixelData, stride, 0);
            Image = BitmapSource.Create(origSrc.PixelWidth, origSrc.PixelHeight, 96, 96, PixelFormats.Bgra32, null, pixelData, stride);
            Reset();
        }

        private IntPtr getAddrOfPixel(int x, int y)
        {
            return NewImage.BackBuffer + y * NewImage.BackBufferStride + x * 4;
        }

        private double getColorDiffMetric(int c1, int c2)
        {
            double a = ((c1 >> 16) & 255) - ((c2 >> 16) & 255);
            double b = ((c1 >> 8) & 255) - ((c2 >> 8) & 255);
            double c = ((c1 >> 0) & 255) - ((c2 >> 0) & 255);
            return Math.Sqrt(a * a + b * b + c * c);
        }

        private unsafe Color getColorOfPixel(int x, int y)
        {
            int color = *((int*)getAddrOfPixel(x, y));
            return Color.FromArgb(
                (byte)((color >> 24) & 255),
                (byte)((color >> 16) & 255),
                (byte)((color >> 8) & 255),
                (byte)((color >> 0) & 255));
        }

        private bool pointInImage(Point p)
        {
            return p.X >= 0 && p.X < NewImage.PixelWidth && p.Y >= 0 && p.Y < NewImage.PixelHeight;
        }

        public void RemoveBackgroundOn(Point startPoint, int treshhold)
        {
            startPoint = new Point((int)startPoint.X, (int)startPoint.Y);
            if (!pointInImage(startPoint)) return;
            NewImage.Lock();
            unsafe
            {
                bool[,] visited = new bool[NewImage.PixelWidth, NewImage.PixelHeight];
                int startColor = *((int*)getAddrOfPixel((int)startPoint.X, (int)startPoint.Y));
                Stack<Point> stack = new Stack<Point>();
                stack.Push(startPoint);
                while (stack.Count > 0)
                {
                    Point p = stack.Pop();
                    if (!pointInImage(p)||visited[(int)p.X, (int)p.Y])
                        continue;
                    visited[(int)p.X, (int)p.Y] = true;
                    IntPtr addr = getAddrOfPixel((int)p.X, (int)p.Y);
                    if (getColorDiffMetric(*((int*)addr), startColor)<=treshhold)
                    {
                        *((int*)addr) = 0;
                        stack.Push(new Point(p.X + 1, p.Y));
                        stack.Push(new Point(p.X - 1, p.Y));
                        stack.Push(new Point(p.X, p.Y + 1));
                        stack.Push(new Point(p.X, p.Y - 1));
                    }
                }
            }
            NewImage.AddDirtyRect(new Int32Rect(0, 0, NewImage.PixelWidth, NewImage.PixelHeight));
            NewImage.Unlock();
        }

        public void Reset()
        {
            NewImage = new WriteableBitmap(Image);
        }

        public void Save(string safeFileName)
        {
            using(FileStream stream = File.Create(safeFileName))
            {
                PngBitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(NewImage));
                enc.Save(stream);
            }
        }
    }
}
